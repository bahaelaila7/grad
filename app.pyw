#!/usr/bin/env pythonw3.3
import sys

from PyQt4 import QtGui

import mainwindow
import sigs


class Example(QtGui.QDialog, mainwindow.Ui_MainWindow):
    def __init__(self):
        super(Example, self).__init__()
        self.window = QtGui.QMainWindow()
        self.MainWindow = mainwindow.Ui_MainWindow(self.window)
        self.mysignals = sigs.populateSignals(self.MainWindow)
        self.window.show()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

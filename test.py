from english.porter import PorterStemmer


def stem(x: str, stopwords={}, unigrams=True, bigrams=False, trigrams=False) -> str:
    p = PorterStemmer()
    last_yield1 = ''
    last_yield2 = ''
    word = ''

    def my_yield(y):
        nonlocal last_yield1, last_yield2
        if unigrams:
            yield y
        if last_yield1 and bigrams:
            yield last_yield1 + y
        if last_yield2 and trigrams:
            yield last_yield2 + last_yield1 + y
        last_yield2 = last_yield1
        last_yield1 = y

    for c in x:
        if c.isalnum() or c in {'@', '$'}:
            if c == '$':
                c = "*dollar*"
            word += c.lower()
        else:
            if word:
                stem_word = p.stem(word, 0, len(word) - 1)
                if stem_word not in stopwords:
                    yield from my_yield(stem_word)
                word = ''
            if c == '.':
                c = '*dot*'
            if c not in stopwords:
                yield from my_yield(c)

    if word:
        stem_word = p.stem(word, 0, len(word) - 1)
        if stem_word not in stopwords:
            yield from my_yield(stem_word)
        word = ''


def getTokens(x: str, bigrams=True, trigrams=True) -> dict:
    tokens = {}
    uni = None
    bi = None
    tri = None
    token = stem(x)
    try:
        bi = next(token)
        tri = next(token)
    except StopIteration as _:
        if bi:
            tokens = {bi: {'c': 1, 'n': {}}}  # c : count , n : next
        return tokens
    for i in token:
        uni = bi
        bi = tri
        tri = i

        try:
            tokens[uni]['c'] += 1
        except KeyError:
            tokens[uni] = {'c': 1, 'n': {}} if bigrams else {'c': 1}
        if bigrams:
            try:
                tokens[uni]['n'][bi]['c'] += 1
            except KeyError:
                tokens[uni]['n'][bi] = {'c': 1, 'n': {}} if trigrams else {'c': 1}
            if trigrams:
                try:
                    tokens[uni]['n'][bi]['n'][tri]['c'] += 1
                except KeyError:
                    tokens[uni]['n'][bi]['n'][tri] = {'c': 1}
    try:
        tokens[bi]['c'] += 1
        if bigrams:
            try:
                tokens[bi]['n'][tri]['c'] += 1
            except KeyError:
                tokens[bi]['n'][tri] = {'c': 1, 'n': {}} if trigrams else {'c': 1}
    except KeyError:
        if trigrams:
            tokens[bi] = {'c': 1, 'n': {tri: {'c': 1, 'n': {}}}}
        elif bigrams:
            tokens[bi] = {'c': 1, 'n': {tri: {'c': 1}}}
        else:
            tokens[bi] = {'c': 1}
    try:
        tokens[tri]['c'] += 1
    except KeyError:
        tokens[tri] = {'c': 1, 'n': {}} if bigrams else {'c': 1}

    return tokens


def getTokeccns(x: str, bigrams=True, trigrams=True, stopwords=[]) -> dict:
    count, nextgram = 0, 1 #indices
    tokens = {}
    uni = None
    bi = None
    tri = None
    token = stem(x, {''.join(stem(_)) for _ in stopwords})
    try:
        bi = next(token)
        tri = next(token)
    except StopIteration as _:
        if bi:
            tokens = {bi: [1]}  #{bi: {'c': 1, 'n': {}}}  # c : count , n : next
        return tokens
    for i in token:
        uni = bi
        bi = tri
        tri = i

        try:
            tokens[uni][count] += 1
        except KeyError:
            tokens[uni] = [1, {}] if bigrams else [1] #{'c': 1, 'n': {}} if bigrams else {'c': 1}
        if bigrams:
            try:
                tokens[uni][nextgram][bi][count] += 1
            except KeyError:
                tokens[uni][nextgram][bi] = [1, {}] if trigrams else [
                    1] #{'c': 1, 'n': {}} if trigrams else {'c': 1}
            if trigrams:
                try:
                    tokens[uni][nextgram][bi][nextgram][tri][count] += 1
                except KeyError:
                    tokens[uni][nextgram][bi][nextgram][tri] = [1]
    try:
        tokens[bi][count] += 1
        if bigrams:
            try:
                tokens[bi][nextgram][tri][count] += 1
            except KeyError:
                tokens[bi][nextgram][tri] = [1, {}] if trigrams else [1]
    except KeyError:
        if trigrams:
            tokens[bi] = [1, {tri: [1, {}]}] #{'c': 1, 'n': {tri: {'c': 1, 'n': {}}}}
        elif bigrams:
            tokens[bi] = [1, {tri: [1]}] #{'c': 1, 'n': {tri: {'c': 1}}}
        else:
            tokens[bi] = [1] #{'c': 1}
    try:
        tokens[tri][count] += 1
    except KeyError:
        tokens[tri] = [1, {}] if bigrams else [1] #{'c': 1, 'n': {}} if bigrams else {'c': 1}

    return tokens


y = '''
Stem -> DB
//(ask for training ?)
SVD test
Stop words
reduction (percentage of SVD reduction)
training -> stem, stopwords, reduce SVD
cosine  ---------------------------------------------> SVM
results
testing ? characteristics



https://www.youtube.com/watch?v=YtcsM2HvnqM#t=44s


--------------
concise version:
from datetime import datetime as dt, timedelta as td
x = dt.now()
d = dt( x.year, x.month, x.day, 12, 0, 0, 0) - x
n = d if d > td( 0 ) else d + td( days = 1 )
'''
x = "hello @there@2/3 mate, hello can I get you a Soda 1992 ?"

#print(getTokens(y,bigrams=False))
stopwords = ['a', 'the', 'testing', 'can', ' ', '@ORGANIZATION1', 'Q']
#print({''.join(stem(st)) for st in stopwords})
#from collections import namedtuple

#ngram = namedtuple('ngram', ['count', 'next'])
#r = ngram(count=1, next={})
#print(r, r.count)
#con=pymongo.Connection()
#con['test'].test.insert({'asd':r})
#print(ngram(*(con['test'].test.find_one({'asd':1})['asd'])))
#con.close()
#print(count)
#print(getTokens(x, stopwords=stopwords))
#print([_ for _ in stem(y,stopwords={'?',' ','a',',','\n','-','*dot*'})])



#@classmethod
def getTockens(cls, x: str, bigrams=True, trigrams=True, stopwords=[]) -> dict:
    count, nextgram = 0, 1 #indices
    tokens = {}
    uni = None
    bi = None
    tri = None
    token = cls.stem(x, {''.join(cls.stem(_)) for _ in stopwords})
    try:
        bi = next(token)
        tri = next(token)
    except StopIteration as _:
        if bi:
            tokens = {bi: [1]}  #{bi: {'c': 1, 'n': {}}}  # c : count , n : next
        return tokens
    for i in token:
        uni = bi
        bi = tri
        tri = i

        try:
            tokens[uni][count] += 1
        except KeyError:
            tokens[uni] = [1, {}] if bigrams else [1] #{'c': 1, 'n': {}} if bigrams else {'c': 1}
        if bigrams:
            try:
                tokens[uni][nextgram][bi][count] += 1
            except KeyError:
                tokens[uni][nextgram][bi] = [1, {}] if trigrams else [
                    1] #{'c': 1, 'n': {}} if trigrams else {'c': 1}
            if trigrams:
                try:
                    tokens[uni][nextgram][bi][nextgram][tri] += 1
                except KeyError:
                    tokens[uni][nextgram][bi][nextgram][tri] = 1
    try:
        tokens[bi][count] += 1
        if bigrams:
            try:
                tokens[bi][nextgram][tri][count] += 1
            except KeyError:
                tokens[bi][nextgram][tri] = [1, {}] if trigrams else [1]
    except KeyError:
        if trigrams:
            tokens[bi] = [1, {tri: [1, {}]}] #{'c': 1, 'n': {tri: {'c': 1, 'n': {}}}}
        elif bigrams:
            tokens[bi] = [1, {tri: [1]}] #{'c': 1, 'n': {tri: {'c': 1}}}
        else:
            tokens[bi] = [1] #{'c': 1}
    try:
        tokens[tri][count] += 1
    except KeyError:
        tokens[tri] = [1, {}] if bigrams else [1] #{'c': 1, 'n': {}} if bigrams else {'c': 1}

    return tokens


def getTokens(x: str, unigrams=True, bigrams=True, trigrams=True, stopwords:list=[]) -> dict:
    tokens = {'uni': {}, 'bi': {}, 'tri': {}}
    uni = None
    bi = None
    tri = None
    token = stem(x, {''.join(stem(_)) for _ in stopwords})
    try:
        bi = next(token)
        tri = next(token)
    except StopIteration as _:
        if bi:
            tokens[uni][bi] = 1
        return tokens

    for i in token:
        uni = bi
        bi = tri
        tri = i
        if unigrams:
            try:
                tokens['uni'][uni] += 1
            except KeyError:
                tokens['uni'][uni] = 1
        if bigrams:
            try:
                tokens['bi'][uni + bi] += 1
            except KeyError:
                tokens['bi'][uni + bi] = 1
        if trigrams:
            try:
                tokens['tri'][uni + bi + tri] += 1
            except KeyError:
                tokens['tri'][uni + bi + tri] = 1
    return tokens


#print(getTokens(x, stopwords=stopwords))
#header = ['1', '2', '3', '4', '5']
#curID = 0
#essaySet = 1
#line = "asdasd"
#print({key: value for key, value in zip(header, (curID, essaySet, line, float("3"), float("4")))})
#print(x)
#for i in stem(x):
#    print(i)


def getp(x, min_rep=1000, save=False):
    from collections import Counter
    from pymongo import Connection
    import itertools

    con = Connection()
    db = con['grad1']
    for k in x:
        pool = Counter()
        for i in db.training.essays.data[k].find(fields=['processed']):
            pool.update(i['processed'])
        pool_list = sorted(pool, key=(lambda key: pool[key]), reverse=True)
        hi = len(pool_list)
        lo = 0
        if pool[pool_list[0]] > min_rep:
            while lo < hi:
                mid = (lo + hi) // 2
                if pool[pool_list[mid]] >= min_rep:
                    lo = mid + 1
                else:
                    hi = mid
            pool_list = [term for term in itertools.islice(pool_list, 0, lo)]
            print(len(pool_list))
        print(k, len(pool_list), lo)
        if save:
            db.training.essays.pool[k].insert({'pool': pool_list})
    con.close()

#getp([str(i) for i in range(1,9)],min_rep=20,save=True)

import numpy
from scipy.sparse import coo_matrix
#def ry(x,y):
#    import random
#    l=[]
#    for i in range(y):
#        l.append(random.randint(1,x))
#    return np.array(l,dtype=np.float32)
#M,N=2000,12000
#size=960
#A=csc_matrix((ry(20,size),(ry(M-1,size),ry(N-1,size))),shape=(M,N),dtype=np.float32)
#from sparsesvd import sparsesvd
#print('here')
#A.todense()
#ut,s,vt=sparsesvd(A,800)
#print(np.linalg.svd(np.mat(A.todense()),full_matrices=0))
#print(linalg.svds(A,min(*A.shape)-1))
#print('here2')
#print(np.linalg.svd(A.todense()))
#print([x for x in ry(10,5)])
#print(np.array([x for x in ry(10,50000)]))
import pymongo
import gridfs
import pickle

con = pymongo.Connection()
db = con['grad1']
essaySet = '1'
pool_list = db.training.essays.pool[essaySet].find_one(fields=['pool'])['pool']
terms_vector = dict(zip(pool_list, range((len(pool_list))))) #assigning indices to terms
rows = []
cols = []
data = []
count = 0
for i in db.training.essays.data[essaySet].find(fields=['processed']):
    for l in i['processed']:
        try:
            index = terms_vector[l]
            rows.append(index)
            cols.append(count)
            data.append(i['processed'][l])
        except KeyError:
            pass
    count += 1

A_matrix = coo_matrix((data, (rows, cols)), shape=(len(terms_vector), count), dtype=numpy.float32)

print('here')
U, S, V = numpy.linalg.svd(A_matrix.todense(), full_matrices=0)
print('here2')

USV = U, S, V
fs = gridfs.GridFS(con['grad1'], 'fs.USV.' + essaySet)
gridin = fs.new_file(filename='USV2')
try:
    gridin.write(pickle.dumps(USV))
except Exception as e:
    print('Error: ', str(e))
finally:
    gridin.close()

gridout = gridfs.GridOut(con.grad1.fs, file_id=con.grad1.fs.USV[essaySet].files.find_one({'filename': 'USV2'})['_id'])
try:
    USV2 = pickle.loads(gridout.read())
except Exception as e:
    print(' Error: ', str(e))
finally:
    gridout.close()
con.close()
for i in zip((m.shape for m in USV), (m.shape for m in USV2)):
    print(i)
#



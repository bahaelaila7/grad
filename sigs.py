from collections import Counter
import itertools
import pickle
import math

import numpy
import gridfs
from PyQt4 import QtCore, QtGui
import pymongo




#from core import RawEssays
import core
import metrics


class TableItem(QtGui.QTableWidgetItem):
    def __init__(self, text):
        super(TableItem, self).__init__(text)
        self.setFlags(QtCore.Qt.ItemFlags(QtCore.Qt.ItemIsSelectable + QtCore.Qt.ItemIsEnabled))


class populateSignals(object):
    def __init__(self, MainWindow):
        self.MainWindow = MainWindow
        self.core = core.AEGCore()
        self.fromDB = None
        self.addSignals(MainWindow)

    def showError(self, title:str, message:str) -> None:
        QtGui.QMessageBox.critical(None, title, message, QtGui.QMessageBox.Ok)

    def showInfo(self, title:str, message:str) -> None:
        QtGui.QMessageBox.information(None, title, message, QtGui.QMessageBox.Ok)

    def setStatus(self, text):
        pass
        #self.MainWindow.statusLbl.setText(text)

    def addSignals(self, mw):
        #mw.dbTrainingSaveBtn.clicked.connect(self.loadFileMenu)
        self.setStatus('Ready')
        #self.MainWindow.progressBar.setVisible(False)
        mw.loadFromFileBtn.clicked.connect(self.loadFileMenu)
        #mw.actionLoad_Test_Data.triggered.connect(self.loadFileMenu)
        mw.dbRefreshBtn.clicked.connect(self.refreshDBs)
        mw.dbTrainingSaveBtn.clicked.connect(self.saveToDB)
        mw.dbTrainingLoadBtn.clicked.connect(self.loadFromDB)
        mw.essaysAnalysisCmb.currentIndexChanged.connect(self.loadSVD)
        mw.applyBtn.clicked.connect(self.applyClassification)
        mw.essaysResultCmb.currentIndexChanged.connect(self.showResults)

    def saveToDB(self):

        if not self.processedFile:
            self.showError('No Current File', 'Please Load a training data file to save to a database')
            return

        def eligible(name: str) -> bool:
            return True   # to be implemented

        db_name = self.MainWindow.dbTrainingNameEdit.text()

        if eligible(db_name):

            db_con = None
            i = None
            try:
                db_con = pymongo.Connection()

                if db_name in db_con.database_names():

                    if QtGui.QMessageBox.question(None, 'DB Exists', "Database %s exists, want to replace?"
                                                                     " click No to go back and change name" % db_name,
                                                  QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
                        db_con.drop_database(db_name)
                    else:
                        return
                db = db_con[db_name]
                db._app.insert({"nameTag": "AEG"})

                stopwords = self.MainWindow.stopwordsTxt.toPlainText().split('\n')
                parse_options = {'stopwords': stopwords,
                                 'unigrams': self.MainWindow.unigramsChk.checkState() == QtCore.Qt.Checked,
                                 'bigrams': self.MainWindow.bigramsChk.checkState() == QtCore.Qt.Checked,
                                 'trigrams': self.MainWindow.trigramsChk.checkState() == QtCore.Qt.Checked,
                                 'min_rep': self.MainWindow.minRepBox.value(),
                                 'randomly': self.MainWindow.chooseRandomlyChk.checkState() == QtCore.Qt.Checked,
                                 'percent': self.MainWindow.testOfTrainingPercent.value(),
                                 'enabled': self.MainWindow.testOfTrainingChk.checkState() == QtCore.Qt.Checked,
                                 'sets': sorted(self.processedFile)}

                db.essays.insert({'options': parse_options})
                #db.essays.insert({'sets': {'keys':sorted(self.processedFile)}})

                stopwords_d = {''.join(self.core.stem(_)) for _ in stopwords}
                #self.MainWindow.progressBar.setVisible(True)
                #self.MainWindow.progressBar.setMaximum(
                #   sum([len(self.processedFile[essaySet]) for essaySet in self.processedFile]))



                #min_rep = self.MainWindow.minRepBox.value()
                progress = 0

                def produceTraining(essays: list, enabled=True, percent=100.0, randomly=True):
                    if not enabled:
                        for i in essays:
                            i['Training'] = True
                            yield i
                    else:
                        essayMarks = {}
                        for i in essays:
                            try:
                                essayMarks[i['mark']].append(i)
                            except KeyError:
                                essayMarks[i['mark']] = [i]
                        import math


                        percent /= 100.0
                        percent = 1.0 - percent #Training

                        def randomlyYield(essays_list, num, remaining):
                            import random

                            i = 0
                            while num and remaining:
                                d = essays_list[i]
                                if random.randint(0, 1):
                                    d['Training'] = True
                                    num -= 1
                                else:
                                    d['Training'] = False
                                    remaining -= 1
                                i += 1
                                yield d
                            while num:
                                d = essays_list[i]
                                d['Training'] = True
                                num -= 1
                                i += 1
                                yield d
                            while remaining:
                                d = essays_list[i]
                                d['Training'] = False
                                remaining -= 1
                                i += 1
                                yield d


                        def myYield(essays_list, num, remaining):
                            for i in range(num):
                                d = essays_list[i]
                                d['Training'] = True
                                yield d
                            for i in range(num, num + remain):
                                d = essays_list[i]
                                d['Training'] = False
                                yield d

                        chosenFunc = randomlyYield if randomly else myYield
                        for mark in essayMarks:
                            essays_list = essayMarks[mark]
                            num = int(math.ceil(len(essays_list) * percent))
                            remain = len(essays_list) - num
                            yield from chosenFunc(essays_list, num, remain)


                for essaySet in sorted(self.processedFile):

                    pool = Counter()
                    pool_docs = Counter()
                    pool_max_tf = {}
                    training_set_size = 0
                    for i in produceTraining(self.processedFile[essaySet], enabled=parse_options['enabled'],
                                             percent=parse_options['percent'],
                                             randomly=parse_options['randomly']):
                        i['processed'] = Counter(
                            self.core.stem(i['essay'], stopwords=stopwords_d, unigrams=parse_options['unigrams'],
                                           bigrams=parse_options['bigrams'], trigrams=['trigrams']))
                        if i['Training']:
                            pool.update(i['processed'])
                            pool_max_tf[i['id']] = i['processed'].most_common(1)[0][1]
                            pool_docs.update(i['processed'].keys())
                            training_set_size += 1
                        db.essays.data[essaySet].insert(i)
                        #progress += 1
                        #self.MainWindow.progressBar.setValue(progress)

                    # creating index
                    db.essays.data[essaySet].ensure_index('id')

                    # term pool
                    pool_list = sorted(pool, key=(lambda key: pool[key]), reverse=True)
                    if parse_options['min_rep'] > 1:
                        if pool[pool_list[0]] > parse_options['min_rep']:
                            hi = len(pool_list)
                            lo = 0
                            while lo < hi:
                                mid = (lo + hi) // 2
                                if pool[pool_list[mid]] >= parse_options['min_rep']:
                                    lo = mid + 1
                                else:
                                    hi = mid
                            pool_list = [term for term in itertools.islice(pool_list, 0, lo)]

                    db.essays.pool[essaySet].insert({'pool': pool_list})

                    del pool
                    #constructing term-document matrix in TF-IDF model

                    terms_vector = dict(zip(pool_list, range((len(pool_list))))) #assigning indices to terms
                    #del pool_list
                    ids = []
                    rows = []
                    cols = []
                    data = []
                    #count = 0

                    for i in db.essays.data[essaySet].find({'Training': True}, fields=['processed', 'id']):
                        ids.append(i['id'])

                        data.append([0.5 if j not in i['processed'] else (
                            0.5 + 0.5 * float(i['processed'][j]) / (pool_max_tf[i['id']] or 1) * math.log(
                                training_set_size / (pool_docs[j] or 1))) for j in pool_list])
                        #for l in i['processed']:

                        #    try:
                        #        index = terms_vector[l]
                        #        rows.append(index)
                        #        cols.append(count)
                        #        #data.append(i['processed'][l])
                        #        data.append(float(i['processed'][l]) / pool_max_tf[i['id']] * math.log(
                        #            training_set_size / (1 + pool_docs[l]), 2))
                        #    except KeyError:
                        #        pass
                        #count += 1

                    del pool_docs, pool_max_tf
                    #print(len(rows),len(cols),len(data))
                    #continue
                    A_matrix = numpy.mat(data, dtype=numpy.float32).T
                    del data
                    #A_matrix = numpy.mat(coo_matrix((data, (rows, cols)),
                    #                                dtype=numpy.float32).todense())




                    #computring SVD
                    #print('*' * 55)
                    SVD = numpy.linalg.svd(A_matrix, full_matrices=False)
                    #print('---' * 15)

                    #saving USV in DB
                    fs = gridfs.GridFS(db)
                    for i in zip('US', SVD[0:2]):
                        grid_in = fs.new_file(filename=i[0] + essaySet)
                        try:
                            grid_in.write(pickle.dumps(i[1]))
                        finally:
                            grid_in.close()
                    grid_in = fs.new_file(filename='A' + essaySet)
                    try:
                        grid_in.write(pickle.dumps(A_matrix))
                    finally:
                        grid_in.close()
                    grid_in = fs.new_file(filename='I' + essaySet)
                    try:
                        grid_in.write(pickle.dumps(ids))
                    finally:
                        grid_in.close()
                    del grid_in, fs
                self.fromDB = db_name
                self.populateAnalysis()
                self.showInfo('Save to Database', 'Done! you can go to the Analysis part now')
                #self.MainWindow.progressBar.setVisible(False)
            except Exception as e:
                #if i: print(i)
                self.showError('Error Connecting to Databases', str(e))
            finally:
                if db_con:
                    db_con.close()
        else:
            self.showError('Error Connecting to Databases', "Invalid Database Name, please remove spaces, dollar signs"
                                                            '($) and dots(.), then try again')


    def refreshDBs(self):
        db_con = None
        try:
            db_con = pymongo.Connection()
            dbs = [db_name for db_name in db_con.database_names() if
                   db_con[db_name]._app.find_one({'nameTag': "AEG"})]

            self.MainWindow.dbNamesCmb.clear()
            self.MainWindow.dbNamesCmb.addItems(dbs)
        except Exception as e:
            self.showError('Error Connecting to Databases', str(e))

        finally:
            if db_con:
                db_con.close()

    def loadSVD(self):
        if self.fromDB:
            con_db = None
            db_name = self.fromDB
            essaySet = self.MainWindow.essaysAnalysisCmb.currentText()
            try:
                con_db = pymongo.Connection()
                db = con_db[db_name]
                grid_out = gridfs.GridOut(db.fs, file_id=db.fs.files.find_one({'filename': 'S' + essaySet})['_id'])

                SS = None
                try:
                    SS = pickle.loads(grid_out.read())
                finally:
                    grid_out.close()

                if SS != None:
                    self.MainWindow.svdFig.clear()

                    minimum = SS[1]
                    SS = numpy.cumsum(SS)

                    norm = SS[-1]
                    self.MainWindow.preserveRankBox.setMinimum(minimum / norm * 100.0)
                    if norm != 0:
                        SS *= 100.0 / norm

                    self.MainWindow.svdFig.grid(True)
                    self.MainWindow.svdFig.plot(range(len(SS)), SS)

                    self.MainWindow.svdPlot.draw()
                else:
                    self.showError('Error Reading from Database',
                                   'Could not retrieve SVD info for essay set: %s' % essaySet)

            except Exception as e:
                self.showError('Error Connecting to Databases', str(e))
            finally:
                if con_db:
                    con_db.close()

        else:
            self.showError('No processed Data', 'Please provide data at the Data tab')

    def populateAnalysis(self):
        self.MainWindow.essaysAnalysisCmb.clear()
        self.MainWindow.essaysAnalysisCmb.addItems(sorted(self.processedFile))

    def loadFromDB(self):
        db_name = self.MainWindow.dbNamesCmb.currentText()
        if db_name:
            db_con = None
            err = None
            try:
                db_con = pymongo.Connection()
                db = db_con[db_name]
                parsing_options = db.essays.find_one(projection={'options': 1})['options']
                sets = parsing_options['sets']
                self.processedFile = {}
                for key in sets:
                    self.processedFile[key] = [x for x in db.essays.data[key].find(projection={'_id': 0})]

                self.MainWindow.stopwordsTxt.setPlainText('\n'.join(parsing_options['stopwords']))
                self.MainWindow.unigramsChk.setCheckState(
                    QtCore.Qt.Checked if parsing_options['unigrams'] else QtCore.Qt.Unchecked)
                self.MainWindow.bigramsChk.setCheckState(
                    QtCore.Qt.Checked if parsing_options['bigrams'] else QtCore.Qt.Unchecked)
                self.MainWindow.trigramsChk.setCheckState(
                    QtCore.Qt.Checked if parsing_options['trigrams'] else QtCore.Qt.Unchecked)
                self.MainWindow.minRepBox.setValue(parsing_options['min_rep'])
                self.MainWindow.testOfTrainingPercent.setValue(parsing_options['percent'])
                self.MainWindow.chooseRandomlyChk.setCheckState(
                    QtCore.Qt.Checked if parsing_options['randomly'] else QtCore.Qt.Unchecked)
                self.MainWindow.testOfTrainingChk.setCheckState(
                    QtCore.Qt.Checked if parsing_options['enabled'] else QtCore.Qt.Unchecked)
                self.MainWindow.dbTrainingNameEdit.setText(db_name)
                self.fromDB = db_name

                self.populateAnalysis()
            except Exception as e:
                self.showError('Error Connecting to Databases', str(e))
                err = True
            finally:
                if db_con:
                    db_con.close()

            if not err:
                pass #self.processedFile = RawEssays(essaySets=essaySets, header=header, lines=lines)

    def showResults(self):
        if self.Results:
            essaySet = self.MainWindow.essaysResultCmb.currentText()
            if essaySet in self.Results:
                my_ratings_list = self.Results[essaySet]['my']
                actual_ratings = self.Results[essaySet]['actual']

                #def frange(init,final,step):
                #    while init<final:
                #        yield init
                #        init+=step
                #plot_range = frange(min(min(my_ratings_list), min(actual_ratings)),
                #                   max(max(my_ratings_list), max(actual_ratings)) + 0.5, 0.5)
                min_rating = min(min(my_ratings_list), min(actual_ratings))
                max_rating = max(max(my_ratings_list), max(actual_ratings))
                actual_ratings, my_ratings_list = zip(*sorted(zip(actual_ratings, my_ratings_list)))
                self.MainWindow.kappaLbl.setText(str(
                    metrics.kappa(actual_ratings, my_ratings_list, min_rating=min_rating, max_rating=max_rating)))
                self.MainWindow.mseLbl.setText(str(math.sqrt(math.fsum(
                    [(my_ratings_list[i] - actual_ratings[i]) ** 2 for i in range(len(actual_ratings))]))))
                plot_range = range(len(actual_ratings))
                self.MainWindow.resultFig.clear()
                self.MainWindow.resultFig.grid(True)
                self.MainWindow.resultFig.plot(plot_range, actual_ratings)
                self.MainWindow.resultFig.hold(True)
                self.MainWindow.resultFig.plot(plot_range, my_ratings_list)

                self.MainWindow.resultPlot.draw()


    def prepareResults(self):
        self.MainWindow.essaysResultCmb.clear()
        self.MainWindow.essaysResultCmb.addItems(sorted(self.Results))

    def AdaBoost(self, *args):
        #def avg(*args):
        ##    for i in zip(*args):
        # #       yield math.fsum(*i)/len(i)
        #def mymax(*args):
        #    for i in zip(*args):
        #        yield max(*i)
        #yield from mymax(*args)
        return [max(*i) for i in zip(*args)]


    def applyClassification(self):
        if self.fromDB:
            db_name = self.fromDB
            db_con = None
            err = False
            try:
                db_con = pymongo.Connection()
                db = db_con[db_name]
                if self.MainWindow.preserveRanks.isChecked():
                    preserve_percentage = self.MainWindow.preserveRankBox.value()
                    #print(preserve_percentage,'-*3'*100)
                else:
                    preserve_percentage = 100.0
                self.Results = {}
                for essaySet in sorted(self.processedFile):


                    USA = {}
                    for i in 'USAI':
                        grid_out = gridfs.GridOut(db.fs,
                                                  file_id=db.fs.files.find_one({'filename': i + essaySet})['_id'])
                        try:
                            USA[i] = pickle.loads(grid_out.read())
                        finally:
                            grid_out.close()

                    if len(USA) == 4:
                        #self.MainWindow.svdFig.clear()

                        if preserve_percentage < 100.0:
                            USA['S'] = numpy.cumsum(USA['S'])
                            norm = USA['S'][-1]
                            if norm != 0:
                                USA['S'] *= 100.0 / norm
                                hi = len(USA['S'])
                                lo = 0
                                while lo < hi:
                                    mid = (lo + hi) >> 1
                                    if USA['S'][mid] < preserve_percentage:
                                        lo = mid + 1
                                    else:
                                        hi = mid

                                if lo > 0:
                                    #for i in USA:
                                    #    print(i,USA[i].shape)

                                    USA['S'] = numpy.mat(numpy.diag(USA['S'][:lo])).I
                                    USA['U'] = numpy.mat(USA['U'][:, :lo]) #.T
                                    rF = USA['U'] * USA['S'] #USA['S']*USA['U']
                                    del USA['S'], USA['U']
                                    USA['A'] = (USA['A'].T * rF).T #rF*USA['A']
                                    #print('A: ', USA['A'].shape)
                    else:
                        self.showError('Error Reading from Database',
                                       'Could not retrieve SVD info for essay set: %s' % essaySet)

                    pool_list = db.essays.pool[essaySet].find_one()['pool']
                    pool = {}.fromkeys(pool_list, 0)
                    terms_vector = dict(zip(pool_list, range(len(pool_list))))
                    #print(len(terms_vector))
                    #del pool_list
                    query = db.essays.data[essaySet].find({'Training': False},
                                                          fields=['processed', 'id'])
                    test_set_size = query.count()
                    pool_max_tf = {}
                    ids = []
                    rows = []
                    cols = []
                    data = []
                    for i in query:
                        for term in i['processed']:
                            max = 0
                            try:
                                index = terms_vector[term]
                                mm = i['processed'][term]
                                if mm > max:
                                    max = mm
                                pool[term] += 1
                            except KeyError:
                                pass
                            pool_max_tf[i['id']] = max

                    count = 0

                    for i in db.essays.data[essaySet].find({'Training': False},
                                                           fields=['processed', 'id']):
                        ids.append(i['id'])

                        data.append([0.5 if j not in i['processed'] else (
                            0.5 + 0.5 * float(i['processed'][j]) / (pool_max_tf[i['id']] or 1 ) * math.log(
                                test_set_size / (pool[j] or 1))) for j in pool_list])
                        #for term in i['processed']:

                        #    try:
                        #        index = terms_vector[term]
                        #        rows.append(index)
                        #        cols.append(count)
                        #        data.append(i['processed'][term])
                        #        #data.append(
                        #        #    i['processed'][term] / (1 + pool_max_tf[i['id']]) * math.log(
                        #        #        test_set_size / (1 + pool[term]), 2))
                        #    except KeyError:
                        #        pass
                        #count += 1

                    Q_matrix = numpy.mat(data, dtype=numpy.float32).T
                    #print(Q_matrix.shape)
                    #Q_matrix = numpy.mat(coo_matrix((data, (rows, cols)),
                    #                                dtype=numpy.float32).todense())

                    if rF is not None:
                        Q_matrix = (Q_matrix.T * rF).T

                    cosineSimilarity = None
                    actual_ratings = [i['mark'] for i in db.essays.data[essaySet].find(
                        {'Training': False, 'id': {'$in': ids}})]
                    my_ratings = {i['id']: i['mark'] for i in db.essays.data[essaySet].find(
                        {'Training': True, 'id': {'$in': USA['I']}})}
                    if self.MainWindow.cosineCorrelationChk.checkState():
                        m = USA['A'].T
                        for i in range(m.shape[0]):
                            fac = numpy.linalg.norm(m[i, :])
                            if fac > 0.0:
                                m[i, :] /= fac

                        m = Q_matrix
                        for i in range(m.shape[1]):
                            fac = numpy.linalg.norm(m[:, i])
                            if fac > 0.0:
                                m[:, i] /= fac

                        del m
                        ResultMatrix = USA['A'].T * Q_matrix

                        #cosineSimilarity = ResultMatrix.max(axis=0)
                        amount = 10
                        markIDsSimilarity = ResultMatrix.argsort(axis=0)[-amount:, :]
                        markSimilarity = numpy.matrix(
                            [[ResultMatrix[markIDsSimilarity[i, j], j] for j in range(markIDsSimilarity.shape[1])] for i
                             in range(markIDsSimilarity.shape[0])], dtype=numpy.float32)
                        markSimilarity /= markSimilarity[-1, :]
                        markSimilaritySum = markSimilarity.sum(axis=0)

                        #print(markSimilaritySum)
                        #print(markSimilarity)
                        #print(markSimilarity.shape)
                        #cosineSimilarity=[markSimilarity[:,i] for i in range(markSimilarity.shape[1])]
                        #marks=
                        #print(marks)
                        #cosineSimilarity = [math.fsum([[my_ratings[k] for k in [USA['I'][markSimilarity[j, i]] for j in range(5)]] for i in range(markSimilarity.shape[1])]) / 5.0 for i in
                        #                    marks]
                        cosineSimilarity = [my_ratings[USA['I'][i]] for i in markIDsSimilarity[-1, :].tolist()[0]]
                        #print(cosineSimilarity)
                        #exit()
                        #numpy.where( == cosineSimilarity[0,i])
                        #marksSimilarity = [len(numpy.where(ResultMatrix[:, i] == cosineSimilarity[0, i])) for i in
                        #                   range(ResultMatrix.shape[1])]
                        #marksSimilarity = [numpy.where(marksSimilarity[i]==cosineSimilarity[0,i]) for i in len(marksSimilarity)]
                        #marksSimilarity = [cosineSimilarity[0,i] for i in range(ResultMatrix.shape[1])]
                        #print(cosineSimilarity)
                        #cosineSimilarity = [USA['I'][i] for i in cosineSimilarity.tolist()[0]]
                    kNNSimilarity=None
                    if self.MainWindow.kNNChk.checkState():
                        kNN = self.MainWindow.kNNBox.value()
                        from pyflann import FLANN
                        from scipy.stats import mode

                        flann = FLANN()
                        results, _ = flann.nn(USA['A'].T, Q_matrix.T, kNN, algorithm="kmeans", branching=32,
                                              iterations=7, checks=16)

                        kNNSimilarity = [mode([ my_ratings[USA['I'][results[i, j]]] for j in range(kNN)])[0][0]
                        for i in range(results.shape[0])]
                        #print(type(results))
                        #print(results.shape)
                        #print(marks)
                        #exit()
                    self.Results[essaySet] = {}
                    self.Results[essaySet]['my'] = self.AdaBoost(kNNSimilarity,cosineSimilarity)
                    self.Results[essaySet]['actual'] = actual_ratings

                self.prepareResults()

            except Exception as e:
                self.showError('Error Connecting to Databases', str(e))
                err = True
            finally:
                if db_con:
                    db_con.close()

    def loadFileMenu(self, e):
        self.setStatus('Loading file')
        filename = QtGui.QFileDialog.getOpenFileName(None, 'Locate Training Data', '.')
        if filename:
            try:
                self.setStatus('Checking File')
                processedFile = self.core.loadDataFile(filename)
            except Exception as e:
                self.showError('Error Reading File', str(e))
                self.setStatus('Loading Failed')
                return

            if processedFile:
                self.MainWindow.trainingFileNameLabel.setText(filename)
                table = self.MainWindow.trainingEssaysSummaryTable
                essaySets = processedFile
                table.clear()
                table.setRowCount(len(essaySets) + 1)
                table.setColumnCount(2)
                table.setItem(0, 0, TableItem('Essay Sets'))
                table.setItem(0, 1, TableItem('Number of Essays'))
                for k, essaySet in enumerate(sorted(essaySets)):
                    table.setItem(k + 1, 0, TableItem(essaySet))
                    table.setItem(k + 1, 1, TableItem(str(len(essaySets[essaySet]))))
                self.processedFile = processedFile
                self.setStatus('Ready')

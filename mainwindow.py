from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure
from PyQt4 import QtGui

import uimainwindow


class Ui_MainWindow(uimainwindow.Ui_MainWindow):
    def __init__(self, MainWindow):
        self.setupUi(MainWindow)
        self.svdFigure = Figure((1.0, 1.0), dpi=96)
        self.svdPlot = FigureCanvas(self.svdFigure)
        self.horizontalLayout__1 = QtGui.QVBoxLayout(self.svdFrame)
        self.svdPlot.setParent(self.svdFrame)
        self.svdNavToolBar = NavigationToolbar(self.svdPlot, self.svdFrame)
        self.horizontalLayout__1.addWidget(self.svdPlot)
        self.horizontalLayout__1.addWidget(self.svdNavToolBar)
        self.svdFig = self.svdFigure.add_subplot(111)
        #------
        self.resultFigure = Figure((1.0, 1.0), dpi=96)
        self.resultPlot = FigureCanvas(self.resultFigure)
        self.horizontalLayout__12 = QtGui.QVBoxLayout(self.resultFrame)
        self.resultPlot.setParent(self.resultFrame)
        self.resultNavToolBar = NavigationToolbar(self.resultPlot, self.resultFrame)
        self.horizontalLayout__12.addWidget(self.resultPlot)
        self.horizontalLayout__12.addWidget(self.resultNavToolBar)
        self.resultFig = self.resultFigure.add_subplot(111)
        #-----
        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setVisible(False)
        self.statusBar.addPermanentWidget(self.progressBar, stretch=0)
        self.progressBar.setMaximumWidth(200)

        #fig.grid = True

        #t = [random.randint(0, 12) for _ in range(1000)]
        #fig.plot(range(len(t)), t)



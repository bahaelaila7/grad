from english.porter import PorterStemmer


class AEGCore():
    def __init__(self):
        self.processedFile = None

    @classmethod
    def stem(cls, x: str, stopwords={}, unigrams=True, bigrams=False, trigrams=False) -> str:
        p = PorterStemmer()
        last_yield1 = ''
        last_yield2 = ''
        word = ''

        def my_yield(y):
            nonlocal last_yield1, last_yield2
            if unigrams:
                yield y
            if last_yield1 and bigrams:
                yield last_yield1 + y
            if last_yield2 and trigrams:
                yield last_yield2 + last_yield1 + y
            last_yield2 = last_yield1
            last_yield1 = y

        for c in x:
            if c.isalnum() or c in {'@', '$'}:
                if c == '$':
                    c = "*dollar*"
                word += c.lower()
            else:
                if word:
                    stem_word = p.stem(word, 0, len(word) - 1)
                    if stem_word not in stopwords:
                        yield from my_yield(stem_word)
                    word = ''
                if c == '.':
                    c = '*dot*'
                if c not in stopwords:
                    yield from my_yield(c)

        if word:
            stem_word = p.stem(word, 0, len(word) - 1)
            if stem_word not in stopwords:
                yield from my_yield(stem_word)
            word = ''


    def loadDataFile(self, file: str) -> dict:
        """

        :param self:
        :param file: filepath
        :return: :raise:
        """
        if file != '':
            with open(file) as f:
                header = f.readline().split('\t')
                if len(header) == 5:
                    essaySets = {}
                    count = 0
                    for line in f:
                        count += 1
                        line = line.split('\t')
                        line[-1] = line[-1].replace('\n', '')
                        if len(line) == 5:
                            essay = {'id': int(line[0]), 'essay': line[2],
                                     'mark': max(float(line[3]) , float(line[4])) }
                            try:
                                essaySets[line[1]].append(essay)
                            except KeyError:
                                essaySets[line[1]] = [essay]

                        else:
                            raise Exception('Number of columns is not 5 at line %d' % (count + 2))
                    self.processedFile = essaySets
                    return self.processedFile
                else:
                    raise Exception('Number of columns is not 5 at header')
        return None
